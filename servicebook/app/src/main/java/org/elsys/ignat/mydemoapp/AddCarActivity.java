package org.elsys.ignat.mydemoapp;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.orm.SugarContext;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.Buffer;
import java.util.List;

public class AddCarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_car);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SugarContext.init(getApplicationContext());

    }
    public void saveCarDB() {
        final EditText EditMaker = (EditText)findViewById(R.id.maker);
        final EditText EditModel = (EditText)findViewById(R.id.model);
        final EditText EditYear = (EditText)findViewById(R.id.year);
        final EditText EditSize = (EditText)findViewById(R.id.engine_size);
        final EditText EditPower = (EditText)findViewById(R.id.horse_power);
        final EditText EditMilleage = (EditText)findViewById(R.id.milleage);
        Car addingCar = new Car(EditMaker.getText().toString(), EditModel.getText().toString()
                , Integer.parseInt(EditYear.getText().toString()), Integer.parseInt(EditSize.getText().toString()), Integer.parseInt(EditPower.getText().toString())
                , Integer.parseInt(EditMilleage.getText().toString()));
        addingCar.save();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.add_menu,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.submit_button) {
            saveCarDB();
            return true;
        }
        if (id == android.R.id.home) {
            this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
