package org.elsys.ignat.mydemoapp;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;

import java.util.Calendar;

public class AddManipulationActivity extends AppCompatActivity {
    ImageButton dateBtn;
    int year_x, day_x, month_x;
    static final int DIALOG_ID = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_manipulation);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Calendar cal = Calendar.getInstance();
        year_x = cal.get(Calendar.YEAR);
        month_x = cal.get(Calendar.MONTH);
        day_x = cal.get(Calendar.DAY_OF_MONTH);
        showDialogOnButtonClick();

    }
    public void showDialogOnButtonClick() {
        dateBtn = (ImageButton)findViewById(R.id.imageButton);
        dateBtn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showDialog(DIALOG_ID);
                    }
                }
        );
    }
    @Override
    protected Dialog onCreateDialog(int id) {
        if (id == DIALOG_ID) {
            return new DatePickerDialog(this, dpickerListener, year_x, month_x, day_x);
        }
        else {
            return null;
        }
    }
    private DatePickerDialog.OnDateSetListener dpickerListener
            = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            year_x = year;
            month_x = monthOfYear+1;
            day_x = dayOfMonth;
        }
    };
    public void savetoDb(int car_id) {
        final EditText editDescription = (EditText)findViewById(R.id.editText2);
        final EditText editPrice = (EditText)findViewById(R.id.editText3);
        final EditText editRepairShopName = (EditText)findViewById(R.id.editText4);
        String date  = day_x + "." + month_x + "." +year_x;
        Manipulation manipulation = null;
        if (editPrice != null && editRepairShopName != null  && editDescription != null ) {
            manipulation = new Manipulation(editRepairShopName.getText().toString(),editDescription.getText().toString(),
                    Float.parseFloat(editPrice.getText().toString()),date,car_id);
        }
        if (manipulation != null) {
            manipulation.save();
        }
        finish();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.add_menu,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.submit_button) {
            Bundle bundle = getIntent().getExtras();
            final String id_car = bundle.getString("id");
            assert id_car != null;
            final int car_id = Integer.parseInt(id_car);
            savetoDb(car_id);
            return true;
        }
        if (id == android.R.id.home) {
            this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
