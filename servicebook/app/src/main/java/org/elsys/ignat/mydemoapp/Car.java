package org.elsys.ignat.mydemoapp;

import com.orm.SugarRecord;


public class Car extends SugarRecord {
    String maker;
    String model;
    int year;
    int engine_size;
    int horse_power;
    int mileage;
    public Car() {}

    public Car(String maker, String model, int year, int engine_size, int horse_power, int mileage) {
        this.maker = maker;
        this.model = model;
        this.year = year;
        this.engine_size = engine_size;
        this.horse_power = horse_power;
        this.mileage = mileage;

    }

    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getEngine_size() {
        return engine_size;
    }

    public void setEngine_size(int engine_size) {
        this.engine_size = engine_size;
    }

    public int getHorse_power() {
        return horse_power;
    }

    public void setHorse_power(int horse_power) {
        this.horse_power = horse_power;
    }

    public int getMileage() {
        return mileage;
    }

    public void setMileage(int mileage) {
        this.mileage = mileage;
    }

}