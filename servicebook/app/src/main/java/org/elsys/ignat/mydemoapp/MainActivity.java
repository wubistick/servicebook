package org.elsys.ignat.mydemoapp;


import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.orm.SchemaGenerator;
import com.orm.SugarContext;
import com.orm.SugarDb;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.List;

public class MainActivity extends AppCompatActivity{
    private static final int READ_REQUEST_CODE = 42;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        SugarContext.init(getApplicationContext());
        SchemaGenerator schemaGenerator = new SchemaGenerator(this);
        schemaGenerator.createDatabase(new SugarDb(this).getDB());
        List<Car> cars = Car.listAll(Car.class);
        String[] carNames = new String[cars.size()];
        for (int i = 0; i < cars.size(); i++) {
            carNames[i] = ( cars.get(i).maker + " " + cars.get(i).model );
        }
        if(!cars.isEmpty()) {
            ListView lv = (ListView)findViewById(R.id.listView);
            assert lv != null;
            lv.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, carNames));
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position,
                                        long id) {
                    Intent intent = new Intent(MainActivity.this, ManipulationsActivity.class);
                    intent.putExtra("id", Integer.toString(position + 1));
                    startActivity(intent);

                }
            });
        }
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        if (fab != null) {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(MainActivity.this, AddCarActivity.class);
                    MainActivity.this.startActivity(intent);
                }
            });
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.export_menu) {
            try {
                exportDB();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        }
        if (id == R.id.import_menu) {
            performFileSearch();
        }

        return super.onOptionsItemSelected(item);
    }
    public static void exportDB() throws IOException {
        String carPath= Environment.getExternalStorageDirectory().getAbsolutePath()+"/cars.csv";
        String manipulationsPath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/manipulations.csv";
        File carsFile = new File(carPath);
        File manipulationsFile = new File (manipulationsPath);
        FileOutputStream carsOS = new FileOutputStream(carsFile);
        FileOutputStream manipulationsOS = new FileOutputStream(manipulationsFile);
        BufferedWriter manipulationsBW = new BufferedWriter(new OutputStreamWriter(manipulationsOS));
        BufferedWriter carsBW = new BufferedWriter(new OutputStreamWriter(carsOS));
        List<Car> cars = Car.listAll(Car.class);
        List<Manipulation> manipulations = Manipulation.listAll(Manipulation.class);
        for (int i = 0; i < cars.size(); i++)
        {
            carsBW.write(cars.get(i).maker+","+cars.get(i).model+","+cars.get(i).year+","+cars.get(i).engine_size+","
                    +cars.get(i).horse_power+","+cars.get(i).mileage);
            carsBW.newLine();
        }
        carsBW.close();
        carsOS.close();
        for(int i = 0; i<manipulations.size();i++)
        {
            manipulationsBW.write(manipulations.get(i).repair_shop_name+","+manipulations.get(i).description+","
                    +manipulations.get(i).price+","+manipulations.get(i).date+","+manipulations.get(i)._id);
            manipulationsBW.newLine();
        }
        manipulationsBW.close();
        manipulationsOS.close();

    }
    public void importDB(Uri uri) throws IOException {
        String path = uri.getPath();
        String[] arr = path.split("\\/");
        arr[arr.length-1] = arr[arr.length-1].split("//:")[0];
        String[] name = arr[arr.length-1]. split(":");
        Toast.makeText(getBaseContext(), name[name.length-1], Toast.LENGTH_LONG).show();
        InputStream inputStream = getContentResolver().openInputStream(uri);
        BufferedReader reader = new BufferedReader(new InputStreamReader(
                inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line);
            stringBuilder.append("\n");
        }
        inputStream.close();
        reader.close();
        String content = stringBuilder.toString();
        String[] contentByLines = content.split("\n");
        if(name[name.length-1].equals("cars.csv")) {
            for(int i = 0; i< contentByLines.length;i++) {
                Car car = new Car();
                String[] columns = contentByLines[i].split(",");
                car.setMaker(columns[0]);
                car.setModel(columns[1]);
                car.setYear( Integer.valueOf(columns[2]));
                car.setEngine_size(Integer.valueOf(columns[3]));
                car.setHorse_power(Integer.valueOf(columns[4]));
                car.setMileage(Integer.valueOf(columns[5]));
                car.save();
            }
        }
        else if(name[name.length-1].equals("manipulations.csv")) {
            for(int i = 0; i<contentByLines.length; i++) {
                Manipulation manipulation = new Manipulation();
                String[] columns = contentByLines[i].split(",");
                manipulation.setRepair_shop_name(columns[0]);
                manipulation.setDescription(columns[1]);
                manipulation.setPrice(Float.valueOf(columns[2]));
                manipulation.setDate(columns[3]);
                manipulation.set_id(Long.valueOf(columns[4]));
                manipulation.save();
            }
        }
    }
    public void performFileSearch() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        startActivityForResult(intent, READ_REQUEST_CODE);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri uri = null;
            if (resultData != null) {
                uri = resultData.getData();
                String path = uri.getPath();
                String[] arr = path.split("\\.");

                if (arr[arr.length-1].equals("csv")) {
                    try {
                        importDB(uri);
                    }   catch(IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }
}
