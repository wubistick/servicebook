package org.elsys.ignat.mydemoapp;

import com.orm.SugarRecord;

/**
 * Created by Ignat on 18.1.2017 г..
 */
public class Manipulation extends SugarRecord {
    String repair_shop_name;
    String description;
    float price;
    String date;
    long _id ;
    public Manipulation () {}
    public Manipulation(String repair_shop_name, String description, float price, String date, long _id) {
        this.repair_shop_name = repair_shop_name;
        this.description = description;
        this.price = price;
        this.date = date;
        this._id = _id;
    }

    public long get_id() {
        return _id;
    }

    public void set_id (long _id) {
        this. _id = _id;
    }

    public String getRepair_shop_name() {
        return repair_shop_name;
    }

    public void setRepair_shop_name(String repair_shop_name) {
        this.repair_shop_name = repair_shop_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
