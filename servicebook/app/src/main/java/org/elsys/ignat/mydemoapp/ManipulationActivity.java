package org.elsys.ignat.mydemoapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.orm.SugarContext;

public class ManipulationActivity extends AppCompatActivity {
    Long manipulationId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manipulation);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SugarContext.init(getApplicationContext());
        Bundle bundle = getIntent().getExtras();
        final String manid = bundle.getString("id");
        manipulationId = Long.valueOf(manid);
        final Manipulation manipulation = Manipulation.findById(Manipulation.class, Long.valueOf(manid));
        TextView Date = (TextView) findViewById(R.id.textView2);
        Date.setText("Date : " + manipulation.getDate());
        TextView Description = (TextView) findViewById(R.id.textView3);
        Description.setText("Description : " + manipulation.getDescription());
        TextView Price = (TextView) findViewById(R.id.textView4);
        String priceString = manipulation.getPrice() + "";
        Price.setText("Price : " + priceString+" leva");
        TextView RepairShopName = (TextView) findViewById(R.id.textView5);
        RepairShopName.setText("Shop Name : " + manipulation.getRepair_shop_name());
        Button submit = (Button) findViewById(R.id.button4);
        if (submit != null) {
            submit.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            manipulation.delete();
                            finish();
                        }
                    }
            );
        }
    }
    @Override
    public void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit_menu,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            this.finish();
            return true;
        }
        if (id == R.id.edit_button) {
            Intent intent = new Intent(ManipulationActivity.this, EditManipulationActivity.class);
            intent.putExtra("id", Long.toString(manipulationId));
            startActivity(intent);
            this.finish();
            return true;

        }
        return super.onOptionsItemSelected(item);
    }
}
