package org.elsys.ignat.mydemoapp;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.orm.SugarContext;
import java.util.List;

public class ManipulationsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manipulations);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SugarContext.init(getApplicationContext());
        Bundle bundle = getIntent().getExtras();
        final String id = bundle.getString("id");
        final List<Manipulation> manipulationsList = Manipulation.find(Manipulation.class, "_id = ?", id);
        String[] manipulations = new String[manipulationsList.size()];
        for (int i = 0; i < manipulationsList.size(); i++) {
            manipulations[i] = ( manipulationsList.get(i).description + " " + manipulationsList.get(i).date );
        }
        if(!manipulationsList.isEmpty()) {
            ListView lv = (ListView)findViewById(R.id.listViewManipulations);
            assert lv != null;
            lv.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, manipulations));
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position,
                                        long id) {
                    Intent intent = new Intent(ManipulationsActivity.this, ManipulationActivity.class);
                    intent.putExtra("id", Long.toString(manipulationsList.get(position).getId()));
                    startActivity(intent);

                }
            });
        }
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        if (fab != null) {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(ManipulationsActivity.this, AddManipulationActivity.class);
                    intent.putExtra("id", id);
                    ManipulationsActivity.this.startActivity(intent);
                }
            });
        }
    }
    @Override
    public void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}


